cmake_minimum_required(VERSION 3.0.0)
project(Tutorial VERSION 0.1.0 LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)

include(CTest)
enable_testing()

add_library(InterfacesLibrary INTERFACE)

target_include_directories(InterfacesLibrary INTERFACE "${CMAKE_SOURCE_DIR}/Interfaces")

add_subdirectory(Service1)
add_subdirectory(Service2)

target_include_directories(Service1 INTERFACE "${CMAKE_SOURCE_DIR}/../Configs")
target_include_directories(Service2 INTERFACE "${CMAKE_SOURCE_DIR}/../Configs")

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
