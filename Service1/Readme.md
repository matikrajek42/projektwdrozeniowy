# Service 1

## Wymagania
- Linux
- Komunikacja dwukierunkowa umożliwiajaca przesylanie poleceń i odpowiedzi (request -> response)
- Łatwy rozwój (dodanie nowych komunikatow)

## Wymagania funkcjonalne
- Wysyła co 2s zapytanie A do serwisu (2) i wyswietla co otrzyma
- po starcie wysyla zapytanie B do serwisu (2) i wyswietla co otrzyma
- co 20s wysyła do serwisu (2) poleceine C z parametrami <ilosc zapytan A wykonanych> <sin(2pi*t)*10>
- po otrzymaniu polecenia ctr+c (zamkniecei aplikacji) wysyła do serwisu (2) polecenia zamkniecia i czeka na zamkniecie serwisu
- nadaje co 1s HB
- wyświetla czy HB z serwisu (2) jest wysyłany poprawnie (co około 1) w przeciwynym razie wyswietla blad
- nasłuchuje za poleceniem E z serwisu (2) i wyswietla jego zawartosc

## Dodatkowe uwagi

Możecie skorzystać z dowolnej technologi[1] ktora jest dostepna na systemie linux. 
Nie ograniczamy was zużyciem RAM'u ani procesora. 
Nie musicie korzystac z żadnej z sugerowanych wczesniej form komunikacji. 
Pracujecie na swoich branchach a gotowe zmiany dodajecie do branchu dev.
Podzial na dwa zespoly każdy odpowiedzialny za swoj serwis/ jeden duży zespół.
Zalecane jest stworzenie 3 aplikacji która bedzię umozliwiała ręczne wysłanie danych oraz odebranie ich.
[1]Gdy wybierzecie kod pisany w c++ zalecamy korzystanie z google code rules.
