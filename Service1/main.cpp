#include <config.h>
#include <sdbus-c++/sdbus-c++.h>
#include <service-client-glue.h>
#include <unistd.h>

#include <atomic>
#include <chrono>
#include <cmath>
#include <csignal>
#include <iostream>
#include <thread>

class Service1Proxy : public sdbus::ProxyInterfaces<rocket::r7::service_proxy> {
 public:
  Service1Proxy(std::string destination, std::string objectPath)
      : ProxyInterfaces(std::move(destination), std::move(objectPath)) {
    this->time = 0;
    registerProxy();
  }
  ~Service1Proxy() { unregisterProxy(); }

  uint32_t time;

 protected:
  virtual void onA(const std::string& dataFromFile,
                   const uint32_t& lenAMethod) override {
    std::cout << "wywołania A:" << lenAMethod << " data from A:" << dataFromFile
              << std::endl;
  }
  virtual void onService2HB(const uint64_t& time) override {
    std::cout << "otrzymano HB od service 2: " << time << std::endl;
  }
};

std::atomic<bool> shouldAStop(false);
std::sig_atomic_t gSignalStatus = 0;

void runAMethodPeriodically(Service1Proxy& service) {
  while (!shouldAStop.load()) {
    service.time += 2;
    try {
      service.A_method();
    } catch (const sdbus::Error& e) {
      std::cerr << "Got concatenate error " << e.getName() << " with message "
                << e.getMessage() << std::endl;
    }

    // Poczekaj 2 sekundy przed następnym wywołaniem metody
    std::this_thread::sleep_for(std::chrono::seconds(2));
  }
}
void runCMethodPeriodically(Service1Proxy& service) {
  while (!shouldAStop.load()) {
    double data;
    data = sin(2 * M_PI * service.time) * 10;
    try {
      service.c(data, service.time);
    } catch (const sdbus::Error& e) {
      std::cerr << "Got concatenate error " << e.getName() << " with message "
                << e.getMessage() << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::seconds(20));
  }
}

void signalHandler(int signal) { gSignalStatus = signal; }

int main(int argc, char* argv[]) {
  Service1Proxy service(config::service2Name, config::service2ObjectPath);
  std::thread aMethodThread(runAMethodPeriodically, std::ref(service));
  std::thread cMethodThread(runCMethodPeriodically, std::ref(service));
  while (gSignalStatus == 0) {
    std::signal(SIGINT, signalHandler);
  }
  shouldAStop.store(true);
  std::cout << "Przechwycono sygnał Ctrl+C." << std::endl;
  bool answer;
  do {
    answer = service.D_methood();
  } while (answer);
  return 0;
}
