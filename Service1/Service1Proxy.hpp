#include <sdbus-c++/sdbus-c++.h>
#include <service-client-glue.h>

#include <iostream>

class Service1Proxy : public sdbus::ProxyInterfaces<rocket::r7::service_proxy> {
 public:
  Service1Proxy(std::string destination, std::string objectPath)
      : ProxyInterfaces(std::move(destination), std::move(objectPath)) {
    this->time = 0;
    registerProxy();
  }
  ~Service1Proxy() { unregisterProxy(); }

  uint32_t time;

 protected:
  virtual void onA(const std::string& dataFromFile,
                   const uint32_t& lenAMethod) override {
    std::cout << "wywołania A:" << lenAMethod << " data from A:" << dataFromFile
              << std::endl;
  }
  virtual void onService2HB(const uint64_t& time) override {
    std::cout << "otrzymano HB od service 2: " << time << std::endl;
  }
};