README.md
1.45 KiB
# Service 2
## Wymagania
- Linux
- Komunikacja dwukierunkowa umożliwiajaca przesylanie poleceń i odpowiedzi (request -> response)
- Łatwy rozwój (dodanie nowych komunikatow)
## Wymagania funkcjonalne
- po otrzymaniu poleceia A(morzecie go sami nazwac czy przypisac mu wlasne id) wysylacie informacje zapisana w pliku a.txt oraz dodatkowo licznik ilosci otrzymanych zapytan A.
- po otrzymaniu polecenia B nadajecie co 1s informacje zapisana w pliku b.bin (uint8 oraz float32)
- po otrzymaniu polecenia C zmieniacie zawartosc pliku na ta podana z poleceniem C
- po otrzymaniu polecenia D kończycie proces
- nadaje co 1s HB
- jesłi z serwisu (1) HB nie nadejdzie co 1 sekunde wyswietla blad i powtarza go co 10s jesli sytułacja się nie zmieni
- wraz z otrzymaniem polecenia C nadaje polecenie E z parametrem zawierajacym ilosc wywołan polecenia a jako liczeb 1 bajtową o zakresie od <-128,127>
## Dodatkowe uwagi
Możecie skorzystać z dowolnej technologi[1] ktora jest dostepna na systemie linux. 
Nie ograniczamy was zużyciem RAM'u ani procesora. 
Nie musicie korzystac z żadnej z sugerowanych wczesniej form komunikacji. 
Pracujecie na swoich branchach a gotowe zmiany dodajecie do branchu dev.
Podzial na dwa zespoly każdy odpowiedzialny za swoj serwis/ jeden duży zespół.
Zalecane jest stworzenie 3 aplikacji która bedzię umozliwiała ręczne wysłanie danych oraz odebranie ich.
[1]Gdy wybierzecie kod pisany w c++ zalecamy korzystanie z google code rules.
