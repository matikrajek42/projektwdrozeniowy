#include <sdbus-c++/sdbus-c++.h>
#include <service-server-glue.h>

#include <atomic>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include "config.h"

std::atomic<bool> shouldStop(false);

class Service2 : public sdbus::AdaptorInterfaces<rocket::r7::service_adaptor> {
 public:
  Service2(sdbus::IConnection& connection, std::string objectPath)
      : AdaptorInterfaces(connection, std::move(objectPath)) {
    registerAdaptor();
    this->lenA = 0;
    this->needRun = true;
  }
  ~Service2() { unregisterAdaptor(); }
  bool getNeedRun() { return this->needRun; }

 protected:
  virtual std::tuple<uint32_t, std::string> A_method() override {
    this->lenA += 1;
    auto tuple = std::make_tuple(this->lenA, this->readDataFromFile());
    std::cout << "[LOG] received A signal" << std::endl;
    emitA(std::get<1>(tuple), std::get<0>(tuple));
    return tuple;
  }
  virtual bool D_methood() override {
    this->needRun = false;
    shouldStop = true;
    return true;
  }
  virtual void c(const double& data, const uint32_t& len) override {
    std::ofstream myFile;
    myFile.open("c.txt");
    if (myFile.is_open()) {
      myFile.clear();
      myFile << len << "\n" << data << "\n";
    }
    myFile.close();
    return;
  }

 private:
  uint32_t lenA;
  bool needRun;
  std::string readDataFromFile() { return "ojasnbdfhinasf"; }
};

void sendHbAsync(Service2& service) {
  uint64_t HBTimes = 0;
  while (!shouldStop.load()) {
    service.emitService2HB(HBTimes);
    HBTimes += 1;
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }
}

auto connection = sdbus::createSystemBusConnection(config::service2Name);
Service2 service(*connection, config::service2ObjectPath);

int main() {
  std::thread HBThread(sendHbAsync, std::ref(service));
  while (service.getNeedRun()) {
    connection->enterEventLoopAsync();
  }
}
